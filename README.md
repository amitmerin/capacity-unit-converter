# Capacity Unit Converter #

This is a basic Typescript utility helping out converting capacity between base 10 values like TeraByte and base 2 values like TebiByte.

## Consuming the package ##

```
#!Typescript

import { CapacityConverter, CapacityUnit } from "capacity-unit-converter";

let converter: CapacityConverter = new CapacityConverter();
let capacity: number = 1;
let sourceUnits: CapacityUnit = CapacityUnit.TeraByte;
let targetUnits: CapacityUnit = CapacityUnit.TebiByte;
converter.convert(capacity, sourceUnits, targetUnits).then((result) => {
  console.log("I Converted = " + result); // = "I Converted = 0.9095"
});
```