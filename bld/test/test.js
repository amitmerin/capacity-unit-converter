"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const chai = require("chai");
const chaiPromised = require("chai-as-promised");
const _ = require("lodash");
const capacity_converter_1 = require('../capacity-converter');
const enums_1 = require('../enums');
//var now = require("performance-now")
chai.use(chaiPromised);
let expect = chai.expect;
describe("My converter tests", () => {
    /*****************************
    * _convertGB_To_GiB
    * ***************************/
    it('should convert gb to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let gb = 1;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGB_To_GiB(gb);
        // Assert
        expect(_.round(result, 2)).to.be.equal(0.93);
    }));
    it('should convert gib to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let gib = 1;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGiB_To_GB(gib);
        // Assert
        expect(result).to.be.equal(1.07);
    }));
    /*****************************
     * _getCapacityInGigaBytes
     * ***************************/
    it('should convert kb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let kb = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGigaBytes(kb, enums_1.CapacityUnit.KiloByte);
        // Assert
        expect(result).to.be.equal(1);
    }));
    it('should convert mb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGigaBytes(size, enums_1.CapacityUnit.MegaByte);
        // Assert
        expect(result).to.be.equal(1000);
    }));
    it('should convert gb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGigaBytes(size, enums_1.CapacityUnit.GigaByte);
        // Assert
        expect(result).to.be.equal(1000000);
    }));
    it('should convert tb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGigaBytes(size, enums_1.CapacityUnit.TeraByte);
        // Assert
        expect(result).to.be.equal(2000);
    }));
    it('should convert pb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGigaBytes(size, enums_1.CapacityUnit.PetaByte);
        // Assert
        expect(result).to.be.equal(2000000);
    }));
    it('_getCapacityInGigaBytes should not onvert kib to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let kib = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        try {
            let result = yield calc._getCapacityInGigaBytes(kib, enums_1.CapacityUnit.KibiByte);
            expect(result).to.not.be.equal(1);
        }
        catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }
    }));
    /*****************************
     * _getCapacityInGibiBytes
     * ***************************/
    it('should convert kib to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1024 * 1024;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.KibiByte);
        // Assert
        expect(result).to.be.equal(1);
    }));
    it('should convert mib to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1024 * 10;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.MebiByte);
        // Assert
        expect(result).to.be.equal(10);
    }));
    it('should convert gib to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.GibiByte);
        // Assert
        expect(result).to.be.equal(1000000);
    }));
    it('should convert tib to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.TebiByte);
        // Assert
        expect(result).to.be.equal(2048);
    }));
    it('should convert pb to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.PebiByte);
        // Assert
        expect(result).to.be.equal(1024 * 1024 * 2);
    }));
    it('_getCapacityInGibiBytes should not convert kb to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        try {
            let result = yield calc._getCapacityInGibiBytes(size, enums_1.CapacityUnit.KiloByte);
            expect(result).to.not.be.equal(1);
        }
        catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }
    }));
    /*****************************
     * _convertGibiBytes
     * ***************************/
    it('should convert GiB to Kib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 3;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGibiBytes(size, enums_1.CapacityUnit.KibiByte);
        // Assert
        expect(result).to.be.equal(1024 * 1024 * 3);
    }));
    it('should convert GiB to Mib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 4;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGibiBytes(size, enums_1.CapacityUnit.MebiByte);
        // Assert
        expect(result).to.be.equal(4 * 1024);
    }));
    it('should convert GiB to GiB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGibiBytes(size, enums_1.CapacityUnit.GibiByte);
        // Assert
        expect(result).to.be.equal(size);
    }));
    it('should convert GiB to TiB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1024;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGibiBytes(size, enums_1.CapacityUnit.TebiByte);
        // Assert
        expect(result).to.be.equal(1);
    }));
    it('should convert GiB to PiB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2 * 1024 * 1024;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGibiBytes(size, enums_1.CapacityUnit.PebiByte);
        // Assert
        expect(result).to.be.equal(2);
    }));
    it('_convertGibiBytes should not convert GiB to gb', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let kib = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        try {
            let result = yield calc._convertGibiBytes(kib, enums_1.CapacityUnit.GigaByte);
            expect(result).to.not.be.equal(1);
        }
        catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }
    }));
    /*****************************
         * _convertGibiBytes
         * ***************************/
    it('should convert GB to KB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 3;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGigaBytes(size, enums_1.CapacityUnit.KiloByte);
        // Assert
        expect(result).to.be.equal(1000 * 1000 * 3);
    }));
    it('should convert GB to MB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 4;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGigaBytes(size, enums_1.CapacityUnit.MegaByte);
        // Assert
        expect(result).to.be.equal(4 * 1000);
    }));
    it('should convert GB to GB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGigaBytes(size, enums_1.CapacityUnit.GigaByte);
        // Assert
        expect(result).to.be.equal(size);
    }));
    it('should convert GB to TB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 1000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGigaBytes(size, enums_1.CapacityUnit.TeraByte);
        // Assert
        expect(result).to.be.equal(1);
    }));
    it('should convert GB to PB', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let size = 2 * 1000 * 1000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc._convertGigaBytes(size, enums_1.CapacityUnit.PetaByte);
        // Assert
        expect(result).to.be.equal(2);
    }));
    it('_convertGigaBytes should not convert GB to gib', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let kib = 1000000;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        //var start = now()
        try {
            let result = yield calc._convertGigaBytes(kib, enums_1.CapacityUnit.GibiByte);
            expect(result).to.not.be.equal(1);
        }
        catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }
    }));
    /****
     *
     * convert(capacity: number, sourceUnits: CapacityUnit, targetUnits: CapacityUnit): Promise<number>
     */
    it('should convert base 10 to base 2', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let GB = 800;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc.convert(GB, enums_1.CapacityUnit.GigaByte, enums_1.CapacityUnit.TebiByte);
        expect(result).to.be.equal(0.7276);
    }));
    it('should convert base 10 to base 10', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let GB = 800;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc.convert(GB, enums_1.CapacityUnit.GigaByte, enums_1.CapacityUnit.TeraByte);
        expect(result).to.be.equal(0.8);
    }));
    it('should convert base 2 to base 10', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let TiB = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc.convert(TiB, enums_1.CapacityUnit.TebiByte, enums_1.CapacityUnit.GigaByte);
        expect(result).to.be.equal(2199.02);
    }));
    it('should convert base 2 to base 2', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let TiB = 2;
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc.convert(TiB, enums_1.CapacityUnit.TebiByte, enums_1.CapacityUnit.GibiByte);
        expect(result).to.be.equal(2048);
    }));
    it('should convert "KiloByte" to CapacityUnit.KiloByte', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let calc = new capacity_converter_1.CapacityConverter();
        // Act
        let result = yield calc.strToCapacityUnit("KiloByte");
        expect(result).to.be.equal(enums_1.CapacityUnit.KiloByte);
    }));
    it('should convert 1000 "KiloByte" to 1024 CapacityUnit.KiloByte', () => __awaiter(this, void 0, void 0, function* () {
        // Arrange
        let calc = new capacity_converter_1.CapacityConverter();
        const unit = "KiloByte";
        // Act
        //let result = await calc.convert(2, CapacityUnit.GigaByte, CapacityUnit.GigaByte);
        // let result = await calc.convert(1000, CapacityUnit.KiloByte, CapacityUnit.KibiByte);
        let result = yield calc.convertToBase2(1000, unit);
        expect(result).to.be.equal(976.5625);
    }));
});
//# sourceMappingURL=test.js.map