"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const math = require('mathjs');
const _ = require("lodash");
const enums_1 = require("./enums");
/**
 * CapacityConverter
 */
class CapacityConverter {
    // constructor(parameters) {
    // }
    convert(capacity, sourceUnits, targetUnits) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var result = 0;
                let sourceIsBase10, sourceIsBase2;
                let targetIsBase10, targetIsBase2;
                if (sourceUnits === enums_1.CapacityUnit.KiloByte ||
                    sourceUnits === enums_1.CapacityUnit.MegaByte ||
                    sourceUnits === enums_1.CapacityUnit.GigaByte ||
                    sourceUnits === enums_1.CapacityUnit.TeraByte ||
                    sourceUnits === enums_1.CapacityUnit.PetaByte) {
                    sourceIsBase10 = true;
                    sourceIsBase2 = false;
                }
                else {
                    sourceIsBase10 = false;
                    sourceIsBase2 = true;
                }
                if (targetUnits === enums_1.CapacityUnit.KiloByte ||
                    targetUnits === enums_1.CapacityUnit.MegaByte ||
                    targetUnits === enums_1.CapacityUnit.GigaByte ||
                    targetUnits === enums_1.CapacityUnit.TeraByte ||
                    targetUnits === enums_1.CapacityUnit.PetaByte) {
                    targetIsBase10 = true;
                    targetIsBase2 = false;
                }
                else {
                    targetIsBase10 = false;
                    targetIsBase2 = true;
                }
                if (sourceIsBase10 && targetIsBase10) {
                    let sourceInGB = yield this._getCapacityInGigaBytes(capacity, sourceUnits);
                    result = yield this._convertGigaBytes(sourceInGB, targetUnits);
                }
                else if (sourceIsBase2 && targetIsBase2) {
                    let sourceInGiB = yield this._getCapacityInGibiBytes(capacity, sourceUnits);
                    result = yield this._convertGibiBytes(sourceInGiB, targetUnits);
                }
                else if (sourceIsBase10 && targetIsBase2) {
                    //change the source to GB
                    let sourceInGB = yield this._getCapacityInGigaBytes(capacity, sourceUnits);
                    //convert GB to GibiByte
                    let sourceInGiB = yield this._convertGB_To_GiB(sourceInGB);
                    //change to target units
                    result = yield this._convertGibiBytes(sourceInGiB, targetUnits);
                }
                else {
                    //sourceIsBase2 && targetIsBase10
                    //change the source to GiB
                    let sourceInGiB = yield this._getCapacityInGibiBytes(capacity, sourceUnits);
                    //convert GiB to GigaByte
                    let sourceInGB = yield this._convertGiB_To_GB(sourceInGiB);
                    //change to target units
                    result = yield this._convertGigaBytes(sourceInGB, targetUnits);
                }
                return new Promise((resolve, reject) => {
                    resolve(_.round(result, 4));
                });
            }
            catch (error) {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
        });
    }
    _convertGB_To_GiB(capacityGB) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let gib = 0;
                    var result = 0;
                    let multiplier = math.pow(10, 9);
                    let divisor = math.pow(2, 30);
                    gib = math.chain(capacityGB).multiply(multiplier).divide(divisor).done();
                    result = gib; //_.round(gib, 2);
                    resolve(result);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    _convertGiB_To_GB(capacityGiB) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let gb = 0;
                    var result = 0;
                    let divisor = math.pow(10, 9);
                    let multiplier = math.pow(2, 30);
                    gb = math.chain(capacityGiB).multiply(multiplier).divide(divisor).done();
                    result = _.round(gb, 2);
                    resolve(result);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    _convertGigaBytes(capacityGB, targetUnits) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let power = 1;
                    switch (targetUnits) {
                        case enums_1.CapacityUnit.KiloByte:
                            power = 6;
                            break;
                        case enums_1.CapacityUnit.MegaByte:
                            power = 3;
                            break;
                        case enums_1.CapacityUnit.GigaByte:
                            resolve(capacityGB);
                            break;
                        case enums_1.CapacityUnit.TeraByte:
                            power = -3;
                            break;
                        case enums_1.CapacityUnit.PetaByte:
                            power = -6;
                            break;
                        case enums_1.CapacityUnit.KibiByte:
                        case enums_1.CapacityUnit.MebiByte:
                        case enums_1.CapacityUnit.GibiByte:
                        case enums_1.CapacityUnit.TebiByte:
                        case enums_1.CapacityUnit.PebiByte:
                            throw "Wrong Conversion!!!";
                        default:
                            power = 1;
                            break;
                    }
                    let gb = 0;
                    var result = 0;
                    let multiplier = math.pow(10, power);
                    gb = math.chain(capacityGB).multiply(multiplier).done();
                    //result = _.round(gb, 2);
                    resolve(gb);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    _getCapacityInGigaBytes(size, units) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let power = 1;
                    switch (units) {
                        case enums_1.CapacityUnit.KiloByte:
                            power = -6;
                            break;
                        case enums_1.CapacityUnit.MegaByte:
                            power = -3;
                            break;
                        case enums_1.CapacityUnit.GigaByte:
                            resolve(size);
                            break;
                        case enums_1.CapacityUnit.TeraByte:
                            power = 3;
                            break;
                        case enums_1.CapacityUnit.PetaByte:
                            power = 6;
                            break;
                        case enums_1.CapacityUnit.KibiByte:
                        case enums_1.CapacityUnit.MebiByte:
                        case enums_1.CapacityUnit.GibiByte:
                        case enums_1.CapacityUnit.TebiByte:
                        case enums_1.CapacityUnit.PebiByte:
                            throw "Wrong Conversion!!!";
                        default:
                            power = 1;
                            break;
                    }
                    let gb = 0;
                    var result = 0;
                    let multiplier = math.pow(10, power);
                    gb = math.chain(size).multiply(multiplier).done();
                    //result = _.round(gb, 2);
                    resolve(gb);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    _convertGibiBytes(capacityGiB, targetUnits) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let power = 1;
                    switch (targetUnits) {
                        case enums_1.CapacityUnit.KibiByte:
                            power = 20;
                            break;
                        case enums_1.CapacityUnit.MebiByte:
                            power = 10;
                            break;
                        case enums_1.CapacityUnit.GibiByte:
                            resolve(capacityGiB);
                            break;
                        case enums_1.CapacityUnit.TebiByte:
                            power = -10;
                            break;
                        case enums_1.CapacityUnit.PebiByte:
                            power = -20;
                            break;
                        case enums_1.CapacityUnit.KiloByte:
                        case enums_1.CapacityUnit.MegaByte:
                        case enums_1.CapacityUnit.GigaByte:
                        case enums_1.CapacityUnit.TeraByte:
                        case enums_1.CapacityUnit.PetaByte:
                            throw "Wrong Conversion!!!";
                        default:
                            power = 1;
                            break;
                    }
                    let gb = 0;
                    var result = 0;
                    let multiplier = math.pow(2, power);
                    gb = math.chain(capacityGiB).multiply(multiplier).done();
                    //result = _.round(gb, 2);
                    resolve(gb);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    _getCapacityInGibiBytes(size, units) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let power = 1;
                    switch (units) {
                        case enums_1.CapacityUnit.KibiByte:
                            power = -20;
                            break;
                        case enums_1.CapacityUnit.MebiByte:
                            power = -10;
                            break;
                        case enums_1.CapacityUnit.GibiByte:
                            resolve(size);
                            break;
                        case enums_1.CapacityUnit.TebiByte:
                            power = 10;
                            break;
                        case enums_1.CapacityUnit.PebiByte:
                            power = 20;
                            break;
                        case enums_1.CapacityUnit.KiloByte:
                        case enums_1.CapacityUnit.MegaByte:
                        case enums_1.CapacityUnit.GigaByte:
                        case enums_1.CapacityUnit.TeraByte:
                        case enums_1.CapacityUnit.PetaByte:
                            throw "Wrong Conversion!!!";
                        default:
                            power = 1;
                            break;
                    }
                    let gb = 0;
                    var result = 0;
                    let multiplier = math.pow(2, power);
                    gb = math.chain(size).multiply(multiplier).done();
                    //result = _.round(gb, 2);
                    resolve(gb);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    /**
     *
     *
     * @param {string} value
     * @returns {Promise<CapacityUnit>}
     *
     * @memberOf CapacityConverter
     */
    strToCapacityUnit(value) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    let result = enums_1.CapacityUnit.KiloByte;
                    switch (value.toLowerCase()) {
                        case "KibiByte".toLowerCase():
                            result = enums_1.CapacityUnit.KibiByte;
                            break;
                        case "MebiByte".toLowerCase():
                            result = enums_1.CapacityUnit.MebiByte;
                            break;
                        case "GibiByte".toLowerCase():
                            result = enums_1.CapacityUnit.GibiByte;
                            break;
                        case "TebiByte".toLowerCase():
                            result = enums_1.CapacityUnit.TebiByte;
                            break;
                        case "PebiByte".toLowerCase():
                            result = enums_1.CapacityUnit.PebiByte;
                            break;
                        case "KiloByte".toLowerCase():
                            result = enums_1.CapacityUnit.KiloByte;
                            break;
                        case "GigaByte".toLowerCase():
                            result = enums_1.CapacityUnit.GigaByte;
                            break;
                        case "TeraByte".toLowerCase():
                            result = enums_1.CapacityUnit.TeraByte;
                            break;
                        case "PetaByte".toLowerCase():
                            result = enums_1.CapacityUnit.PetaByte;
                            break;
                        default:
                            throw "Wrong Conversion!!!";
                    }
                    resolve(result);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    }
    /**
     * Converts base 10 units to base 2 units
     *
     * @param {number} capacity
     * @param {string} units
     * @returns {Promise<number>}
     *
     * @memberOf CapacityConverter
     */
    convertToBase2(capacity, units) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sourceUnits = yield this.strToCapacityUnit(units);
                let targetUnits = enums_1.CapacityUnit.KibiByte;
                switch (sourceUnits) {
                    case enums_1.CapacityUnit.KiloByte:
                        targetUnits = enums_1.CapacityUnit.KibiByte;
                        break;
                    case enums_1.CapacityUnit.MegaByte:
                        targetUnits = enums_1.CapacityUnit.MebiByte;
                        break;
                    case enums_1.CapacityUnit.GigaByte:
                        targetUnits = enums_1.CapacityUnit.GibiByte;
                        break;
                    case enums_1.CapacityUnit.TeraByte:
                        targetUnits = enums_1.CapacityUnit.TebiByte;
                        break;
                    case enums_1.CapacityUnit.PetaByte:
                        targetUnits = enums_1.CapacityUnit.PebiByte;
                        break;
                    default:
                        throw "Wrong Conversion!!!";
                }
                var result = yield this.convert(capacity, sourceUnits, targetUnits);
                return new Promise((resolve, reject) => {
                    try {
                        resolve(result);
                    }
                    catch (error) {
                        reject(error);
                    }
                });
            }
            catch (error) {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
        });
    }
}
exports.CapacityConverter = CapacityConverter;
//# sourceMappingURL=capacity-converter.js.map