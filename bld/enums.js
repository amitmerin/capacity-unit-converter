"use strict";
(function (CapacityUnit) {
    CapacityUnit[CapacityUnit["KiloByte"] = 0] = "KiloByte";
    CapacityUnit[CapacityUnit["KibiByte"] = 1] = "KibiByte";
    CapacityUnit[CapacityUnit["MegaByte"] = 2] = "MegaByte";
    CapacityUnit[CapacityUnit["MebiByte"] = 3] = "MebiByte";
    CapacityUnit[CapacityUnit["GigaByte"] = 4] = "GigaByte";
    CapacityUnit[CapacityUnit["GibiByte"] = 5] = "GibiByte";
    CapacityUnit[CapacityUnit["TeraByte"] = 6] = "TeraByte";
    CapacityUnit[CapacityUnit["TebiByte"] = 7] = "TebiByte";
    CapacityUnit[CapacityUnit["PetaByte"] = 8] = "PetaByte";
    CapacityUnit[CapacityUnit["PebiByte"] = 9] = "PebiByte";
})(exports.CapacityUnit || (exports.CapacityUnit = {}));
var CapacityUnit = exports.CapacityUnit;
//# sourceMappingURL=enums.js.map