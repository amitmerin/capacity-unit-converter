//import * as chai from "chai"
import * as mocha from "mocha"
import * as chai from "chai"
import * as chaiPromised from "chai-as-promised"
import * as _ from "lodash";

import { CapacityConverter } from '../capacity-converter'
import { CapacityUnit } from '../enums'

//var now = require("performance-now")
chai.use(chaiPromised);
let expect = chai.expect;

describe("My converter tests", () => {
    /*****************************
    * _convertGB_To_GiB
    * ***************************/
    it('should convert gb to gib', async () => {

        // Arrange
        let gb = 1;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._convertGB_To_GiB(gb);

        // Assert
        expect(_.round(result,2)).to.be.equal(0.93);

    })
    it('should convert gib to gb', async () => {

        // Arrange
        let gib = 1;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._convertGiB_To_GB(gib);

        // Assert
        expect(result).to.be.equal(1.07);

    })
    /*****************************
     * _getCapacityInGigaBytes
     * ***************************/
    it('should convert kb to gb', async () => {

        // Arrange
        let kb = 1000000;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._getCapacityInGigaBytes(kb, CapacityUnit.KiloByte);

        // Assert
        expect(result).to.be.equal(1);

    })
    it('should convert mb to gb', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._getCapacityInGigaBytes(size, CapacityUnit.MegaByte);

        // Assert
        expect(result).to.be.equal(1000);

    })
    it('should convert gb to gb', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGigaBytes(size, CapacityUnit.GigaByte);

        // Assert
        expect(result).to.be.equal(1000000);

    })
    it('should convert tb to gb', async () => {

        // Arrange
        let size = 2;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._getCapacityInGigaBytes(size, CapacityUnit.TeraByte);

        // Assert
        expect(result).to.be.equal(2000);

    })
    it('should convert pb to gb', async () => {

        // Arrange
        let size = 2;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGigaBytes(size, CapacityUnit.PetaByte);

        // Assert
        expect(result).to.be.equal(2000000);

    })
    it('_getCapacityInGigaBytes should not onvert kib to gb', async () => {

        // Arrange
        let kib = 1000000;
        let calc = new CapacityConverter();

        // Act
        try {
            let result = await calc._getCapacityInGigaBytes(kib, CapacityUnit.KibiByte);
            expect(result).to.not.be.equal(1);
        } catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }

    })
    /*****************************
     * _getCapacityInGibiBytes
     * ***************************/
    it('should convert kib to gib', async () => {

        // Arrange
        let size = 1024 * 1024;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.KibiByte);

        // Assert
        expect(result).to.be.equal(1);

    })
    it('should convert mib to gib', async () => {

        // Arrange
        let size = 1024 * 10;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.MebiByte);

        // Assert
        expect(result).to.be.equal(10);

    })
    it('should convert gib to gib', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.GibiByte);

        // Assert
        expect(result).to.be.equal(1000000);

    })
    it('should convert tib to gib', async () => {

        // Arrange
        let size = 2;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.TebiByte);

        // Assert
        expect(result).to.be.equal(2048);

    })
    it('should convert pb to gb', async () => {

        // Arrange
        let size = 2;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.PebiByte);

        // Assert
        expect(result).to.be.equal(1024 * 1024 * 2);

    })
    it('_getCapacityInGibiBytes should not convert kb to gib', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();

        // Act
        try {
            let result = await calc._getCapacityInGibiBytes(size, CapacityUnit.KiloByte);
            expect(result).to.not.be.equal(1);
        } catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }

    })
    /*****************************
     * _convertGibiBytes
     * ***************************/
    it('should convert GiB to Kib', async () => {

        // Arrange
        let size = 3;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGibiBytes(size, CapacityUnit.KibiByte);

        // Assert
        expect(result).to.be.equal(1024 * 1024 * 3);

    })
    it('should convert GiB to Mib', async () => {

        // Arrange
        let size = 4;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGibiBytes(size, CapacityUnit.MebiByte);

        // Assert
        expect(result).to.be.equal(4 * 1024);

    })
    it('should convert GiB to GiB', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGibiBytes(size, CapacityUnit.GibiByte);

        // Assert
        expect(result).to.be.equal(size);

    })
    it('should convert GiB to TiB', async () => {

        // Arrange
        let size = 1024;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGibiBytes(size, CapacityUnit.TebiByte);

        // Assert
        expect(result).to.be.equal(1);

    })
    it('should convert GiB to PiB', async () => {

        // Arrange
        let size = 2 * 1024 * 1024;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGibiBytes(size, CapacityUnit.PebiByte);

        // Assert
        expect(result).to.be.equal(2);

    })
    it('_convertGibiBytes should not convert GiB to gb', async () => {

        // Arrange
        let kib = 1000000;
        let calc = new CapacityConverter();

        // Act
        try {
            let result = await calc._convertGibiBytes(kib, CapacityUnit.GigaByte);
            expect(result).to.not.be.equal(1);
        } catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }

    })
    /*****************************
         * _convertGibiBytes
         * ***************************/
    it('should convert GB to KB', async () => {

        // Arrange
        let size = 3;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGigaBytes(size, CapacityUnit.KiloByte);

        // Assert
        expect(result).to.be.equal(1000 * 1000 * 3);

    })
    it('should convert GB to MB', async () => {

        // Arrange
        let size = 4;
        let calc = new CapacityConverter();

        // Act
        let result = await calc._convertGigaBytes(size, CapacityUnit.MegaByte);

        // Assert
        expect(result).to.be.equal(4 * 1000);

    })
    it('should convert GB to GB', async () => {

        // Arrange
        let size = 1000000;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._convertGigaBytes(size, CapacityUnit.GigaByte);

        // Assert
        expect(result).to.be.equal(size);

    })
    it('should convert GB to TB', async () => {

        // Arrange
        let size = 1000;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._convertGigaBytes(size, CapacityUnit.TeraByte);

        // Assert
        expect(result).to.be.equal(1);

    })
    it('should convert GB to PB', async () => {

        // Arrange
        let size = 2 * 1000 * 1000;
        let calc = new CapacityConverter();


        // Act
        let result = await calc._convertGigaBytes(size, CapacityUnit.PetaByte);

        // Assert
        expect(result).to.be.equal(2);

    })
    it('_convertGigaBytes should not convert GB to gib', async () => {

        // Arrange
        let kib = 1000000;
        let calc = new CapacityConverter();


        // Act
        //var start = now()
        try {
            let result = await calc._convertGigaBytes(kib, CapacityUnit.GibiByte);
            expect(result).to.not.be.equal(1);
        } catch (error) {
            expect(error).to.be.equal("Wrong Conversion!!!");
        }

    })
    /****
     * 
     * convert(capacity: number, sourceUnits: CapacityUnit, targetUnits: CapacityUnit): Promise<number>
     */
    it('should convert base 10 to base 2', async () => {

        // Arrange
        let GB = 800;
        let calc = new CapacityConverter();

        // Act
        let result = await calc.convert(GB, CapacityUnit.GigaByte, CapacityUnit.TebiByte);
        expect(result).to.be.equal(0.7276);


    })
    it('should convert base 10 to base 10', async () => {

        // Arrange
        let GB = 800;
        let calc = new CapacityConverter();

        // Act
        let result = await calc.convert(GB, CapacityUnit.GigaByte, CapacityUnit.TeraByte);
        expect(result).to.be.equal(0.8);


    })
      it('should convert base 2 to base 10', async () => {

        // Arrange
        let TiB = 2;
        let calc = new CapacityConverter();

        // Act
        let result = await calc.convert(TiB, CapacityUnit.TebiByte, CapacityUnit.GigaByte);
        expect(result).to.be.equal(2199.02);


    })
    it('should convert base 2 to base 2', async () => {

        // Arrange
        let TiB = 2;
        let calc = new CapacityConverter();

        // Act
        let result = await calc.convert(TiB, CapacityUnit.TebiByte, CapacityUnit.GibiByte);
        expect(result).to.be.equal(2048);


    })
    it('should convert "KiloByte" to CapacityUnit.KiloByte', async () => {

        // Arrange
        let calc = new CapacityConverter();

        // Act
        let result = await calc.strToCapacityUnit("KiloByte");
        expect(result).to.be.equal(CapacityUnit.KiloByte);


    })
    it('should convert 1000 "KiloByte" to 1024 CapacityUnit.KiloByte', async () => {

        // Arrange
        let calc = new CapacityConverter();
        const unit:string = "KiloByte";
        // Act
        //let result = await calc.convert(2, CapacityUnit.GigaByte, CapacityUnit.GigaByte);
        // let result = await calc.convert(1000, CapacityUnit.KiloByte, CapacityUnit.KibiByte);
         let result = await calc.convertToBase2(1000, unit);
        expect(result).to.be.equal(976.5625);


    })
});