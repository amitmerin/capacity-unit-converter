import * as math from 'mathjs';
import * as _ from "lodash";

import { CapacityUnit } from "./enums";
/**
 * CapacityConverter
 */
export class CapacityConverter {
    // constructor(parameters) {

    // }

    async convert(capacity: number, sourceUnits: CapacityUnit, targetUnits: CapacityUnit): Promise<number> {

        try {
            var result: number = 0;
            let sourceIsBase10: boolean, sourceIsBase2: boolean;
            let targetIsBase10: boolean, targetIsBase2: boolean;
            if (sourceUnits === CapacityUnit.KiloByte ||
                sourceUnits === CapacityUnit.MegaByte ||
                sourceUnits === CapacityUnit.GigaByte ||
                sourceUnits === CapacityUnit.TeraByte ||
                sourceUnits === CapacityUnit.PetaByte) {
                sourceIsBase10 = true;
                sourceIsBase2 = false;
            } else {
                sourceIsBase10 = false;
                sourceIsBase2 = true;
            }

            if (targetUnits === CapacityUnit.KiloByte ||
                targetUnits === CapacityUnit.MegaByte ||
                targetUnits === CapacityUnit.GigaByte ||
                targetUnits === CapacityUnit.TeraByte ||
                targetUnits === CapacityUnit.PetaByte) {
                targetIsBase10 = true;
                targetIsBase2 = false;
            } else {
                targetIsBase10 = false;
                targetIsBase2 = true;
            }

            if (sourceIsBase10 && targetIsBase10) {
                let sourceInGB: number = await this._getCapacityInGigaBytes(capacity, sourceUnits);
                result = await this._convertGigaBytes(sourceInGB, targetUnits);

            }
            else if (sourceIsBase2 && targetIsBase2) {
                let sourceInGiB: number = await this._getCapacityInGibiBytes(capacity, sourceUnits);
                result = await this._convertGibiBytes(sourceInGiB, targetUnits);

            }
            else if (sourceIsBase10 && targetIsBase2) {
                //change the source to GB
                let sourceInGB: number = await this._getCapacityInGigaBytes(capacity, sourceUnits);
                //convert GB to GibiByte
                let sourceInGiB: number = await this._convertGB_To_GiB(sourceInGB);
                //change to target units
                result = await this._convertGibiBytes(sourceInGiB, targetUnits);

            }
            else {
                //sourceIsBase2 && targetIsBase10
                //change the source to GiB
                let sourceInGiB: number = await this._getCapacityInGibiBytes(capacity, sourceUnits);
                //convert GiB to GigaByte
                let sourceInGB: number = await this._convertGiB_To_GB(sourceInGiB);
                //change to target units
                result = await this._convertGigaBytes(sourceInGB, targetUnits);

            }

            return new Promise<number>((resolve, reject) => {
                resolve(_.round(result, 4));
            });
        }
        catch (error) {
            return new Promise<number>((resolve, reject) => {
                reject(error);
            });
        }


    }

    async _convertGB_To_GiB(capacityGB:number): Promise<number> {

        return new Promise<number>((resolve, reject) => {

            try {

                let gib: number = 0;
                var result: number = 0;
                let multiplier = math.pow(10, 9);
                let divisor = math.pow(2, 30);

                gib = math.chain(capacityGB).multiply(multiplier).divide(divisor).done();
                result = gib;//_.round(gib, 2);
                resolve(result);
            } catch (error) {
                reject(error);
            }

        });
    }

    async _convertGiB_To_GB(capacityGiB): Promise<number> {

        return new Promise<number>((resolve, reject) => {

            try {

                let gb: number = 0;
                var result: number = 0;
                let divisor = math.pow(10, 9);
                let multiplier = math.pow(2, 30);

                gb = math.chain(capacityGiB).multiply(multiplier).divide(divisor).done();
                result = _.round(gb, 2);
                resolve(result);
            } catch (error) {
                reject(error);
            }

        });
    }
    async _convertGigaBytes(capacityGB: number, targetUnits: CapacityUnit): Promise<number> {

        return new Promise<number>((resolve, reject) => {

            try {

                let power: number = 1;
                switch (targetUnits) {
                    case CapacityUnit.KiloByte:
                        power = 6;
                        break;
                    case CapacityUnit.MegaByte:
                        power = 3;
                        break;
                    case CapacityUnit.GigaByte:
                        resolve(capacityGB);
                        break;
                    case CapacityUnit.TeraByte:
                        power = -3;
                        break;
                    case CapacityUnit.PetaByte:
                        power = -6;
                        break;
                    case CapacityUnit.KibiByte:
                    case CapacityUnit.MebiByte:
                    case CapacityUnit.GibiByte:
                    case CapacityUnit.TebiByte:
                    case CapacityUnit.PebiByte:
                        throw "Wrong Conversion!!!";

                    default:
                        power = 1;
                        break;
                }

                let gb: number = 0;
                var result: number = 0;

                let multiplier = math.pow(10, power);

                gb = math.chain(capacityGB).multiply(multiplier).done();
                //result = _.round(gb, 2);
                resolve(gb);
            } catch (error) {
                reject(error);
            }

        });
    }
    async _getCapacityInGigaBytes(size: number, units: CapacityUnit): Promise<number> {

        return new Promise<number>((resolve, reject) => {

            try {

                let power: number = 1;
                switch (units) {
                    case CapacityUnit.KiloByte:
                        power = -6;
                        break;
                    case CapacityUnit.MegaByte:
                        power = -3;
                        break;
                    case CapacityUnit.GigaByte:
                        resolve(size);
                        break;
                    case CapacityUnit.TeraByte:
                        power = 3;
                        break;
                    case CapacityUnit.PetaByte:
                        power = 6;
                        break;
                    case CapacityUnit.KibiByte:
                    case CapacityUnit.MebiByte:
                    case CapacityUnit.GibiByte:
                    case CapacityUnit.TebiByte:
                    case CapacityUnit.PebiByte:
                        throw "Wrong Conversion!!!";

                    default:
                        power = 1;
                        break;
                }

                let gb: number = 0;
                var result: number = 0;

                let multiplier = math.pow(10, power);

                gb = math.chain(size).multiply(multiplier).done();
                //result = _.round(gb, 2);
                resolve(gb);
            } catch (error) {
                reject(error);
            }

        });
    }

    async _convertGibiBytes(capacityGiB: number, targetUnits: CapacityUnit): Promise<number> {
        return new Promise<number>((resolve, reject) => {

            try {

                let power: number = 1;
                switch (targetUnits) {
                    case CapacityUnit.KibiByte:
                        power = 20;
                        break;
                    case CapacityUnit.MebiByte:
                        power = 10;
                        break;
                    case CapacityUnit.GibiByte:
                        resolve(capacityGiB);
                        break;
                    case CapacityUnit.TebiByte:
                        power = -10;
                        break;
                    case CapacityUnit.PebiByte:
                        power = -20;
                        break;
                    case CapacityUnit.KiloByte:
                    case CapacityUnit.MegaByte:
                    case CapacityUnit.GigaByte:
                    case CapacityUnit.TeraByte:
                    case CapacityUnit.PetaByte:
                        throw "Wrong Conversion!!!";

                    default:
                        power = 1;
                        break;
                }

                let gb: number = 0;
                var result: number = 0;

                let multiplier = math.pow(2, power);

                gb = math.chain(capacityGiB).multiply(multiplier).done();
                //result = _.round(gb, 2);
                resolve(gb);
            } catch (error) {
                reject(error);
            }

        });
    }
    async _getCapacityInGibiBytes(size: number, units: CapacityUnit): Promise<number> {

        return new Promise<number>((resolve, reject) => {

            try {

                let power: number = 1;
                switch (units) {
                    case CapacityUnit.KibiByte:
                        power = -20;
                        break;
                    case CapacityUnit.MebiByte:
                        power = -10;
                        break;
                    case CapacityUnit.GibiByte:
                        resolve(size);
                        break;
                    case CapacityUnit.TebiByte:
                        power = 10;
                        break;
                    case CapacityUnit.PebiByte:
                        power = 20;
                        break;
                    case CapacityUnit.KiloByte:
                    case CapacityUnit.MegaByte:
                    case CapacityUnit.GigaByte:
                    case CapacityUnit.TeraByte:
                    case CapacityUnit.PetaByte:
                        throw "Wrong Conversion!!!";

                    default:
                        power = 1;
                        break;
                }

                let gb: number = 0;
                var result: number = 0;

                let multiplier = math.pow(2, power);

                gb = math.chain(size).multiply(multiplier).done();
                //result = _.round(gb, 2);
                resolve(gb);
            } catch (error) {
                reject(error);
            }

        });
    }

    /**
     * 
     * 
     * @param {string} value
     * @returns {Promise<CapacityUnit>}
     * 
     * @memberOf CapacityConverter
     */
    async strToCapacityUnit(value: string): Promise<CapacityUnit> {
        return new Promise<CapacityUnit>((resolve, reject) => {

            try {
                let result: CapacityUnit = CapacityUnit.KiloByte;

                switch (value.toLowerCase()) {
                    case "KibiByte".toLowerCase():
                        result = CapacityUnit.KibiByte;
                        break;
                    case "MebiByte".toLowerCase():
                        result = CapacityUnit.MebiByte;
                        break;
                    case "GibiByte".toLowerCase():
                        result = CapacityUnit.GibiByte;
                        break;
                    case "TebiByte".toLowerCase():
                        result = CapacityUnit.TebiByte;
                        break;
                    case "PebiByte".toLowerCase():
                        result = CapacityUnit.PebiByte;
                        break;
                    case "KiloByte".toLowerCase():
                        result = CapacityUnit.KiloByte;
                        break;
                    case "GigaByte".toLowerCase():
                        result = CapacityUnit.GigaByte;
                        break;
                    case "TeraByte".toLowerCase():
                        result = CapacityUnit.TeraByte;
                        break;
                    case "PetaByte".toLowerCase():
                        result = CapacityUnit.PetaByte;
                        break;
                    default:
                        throw "Wrong Conversion!!!";

                }

                resolve(result);
            } catch (error) {
                reject(error);
            }

        });
    }

    /**
     * Converts base 10 units to base 2 units
     * 
     * @param {number} capacity
     * @param {string} units
     * @returns {Promise<number>}
     * 
     * @memberOf CapacityConverter
     */
    async convertToBase2(capacity: number, units: string): Promise<number> {

        try {
            let sourceUnits: CapacityUnit = await this.strToCapacityUnit(units);
            let targetUnits: CapacityUnit = CapacityUnit.KibiByte;

            switch (sourceUnits) {
                case CapacityUnit.KiloByte:
                    targetUnits = CapacityUnit.KibiByte;
                    break;
                case CapacityUnit.MegaByte:
                    targetUnits = CapacityUnit.MebiByte;
                    break;
                case CapacityUnit.GigaByte:
                    targetUnits = CapacityUnit.GibiByte;
                    break;
                case CapacityUnit.TeraByte:
                    targetUnits = CapacityUnit.TebiByte;
                    break;
                case CapacityUnit.PetaByte:
                    targetUnits = CapacityUnit.PebiByte;
                    break;
                default:
                    throw "Wrong Conversion!!!";
            }

            var result: number = await this.convert(capacity, sourceUnits, targetUnits);

            return new Promise<number>((resolve, reject) => {

                try {
                    resolve(result);
                } catch (error) {
                    reject(error);
                }

            });
        } catch (error) {
            return new Promise<number>((resolve, reject) => {
                reject(error);
            });
        }

    }
}