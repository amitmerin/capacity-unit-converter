export enum CapacityUnit {
	KiloByte, 
    KibiByte, 
    MegaByte,
    MebiByte,
    GigaByte,
    GibiByte,
    TeraByte,
    TebiByte,
    PetaByte,
    PebiByte
}